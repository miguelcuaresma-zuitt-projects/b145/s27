//2.

db.fruits.aggregate(
	[
		{
			$match: {onSale: true}
		},
		{
			$count: "fruits on sale"
		}
	]
);

//3.

db.fruits.aggregate(
	[
		{
			$match: {stock: {$gte: 20}}
		},
		{
			$count: "Stocks more than 20"
		}
	]
);

//4.

db.fruits.aggregate(
	[
		{
			$match: {onSale: true}
		},
		{
			$group: {
				_id: null,
				average_price: {$avg: "$price"}
			}
		}
	]
);

//5.
db.fruits.aggregate(
	[
		{
			$match: {onSale: true}
		},
		{
			$group: {
				_id: null,
				maximum: {$max: "$price"}
			}
		}
	]
);

//6.
db.fruits.aggregate(
	[
		{
			$match: {onSale: true}
		},
		{
			$group: {
				_id: null,
				minimum: {$min: "$price"}
			}
		}
	]
);



